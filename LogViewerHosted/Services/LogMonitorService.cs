﻿using LogViewerHosted.Models;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewerHosted.Services
{
    public class LogMonitorService
    {
        public string LogFile { get; }
        
        private Stack<LogEvent> LogEvents { get; } = new Stack<LogEvent>();

        public string[] LogNames { get; private set; }

        public string[] LogLevels { get; private set; }

        private object lockingObject = new object();

        private Task reloadTask = null;

        private DateTime lastReloadTime;

        private long currentLogPosition = 0;

        private IFileProvider fileProvider;

        public LogMonitorService(string logFile, IFileProvider fileProvider)
        {
            LogFile = logFile;

            this.fileProvider = fileProvider;
        }

        public async Task<LogEvent[]> GetLogEventsAsync()
        {
            lock (lockingObject)
            {
                if (reloadTask?.IsCompleted ?? true)
                {
                    if (lastReloadTime.AddSeconds(1) >= DateTime.Now)
                    {
                        return LogEvents.ToArray();
                    }

                    reloadTask = new Task(LoadLatestLogRows);

                    reloadTask.Start();
                }
            }

            await reloadTask;

            return LogEvents.ToArray();
        }

        /// <summary>
        /// Loads the latest log rows from the log file. This is done by using the last read position to ensure only the latest
        /// log items are read back. Anything that has already been parsed will be skipped.
        /// </summary>
        private void LoadLatestLogRows()
        {
            try
            {
                if (!File.Exists(LogFile))
                {
                    Console.WriteLine($"Could not find log file: {LogFile}");
                    Console.WriteLine("If this log file exists check its permissions...");

                    return;
                }

                var linesRead = 0;
                var linesFailed = 0;

                using (var stream = File.OpenRead(LogFile))
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    stream.Position = currentLogPosition;

                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();

                        if (LogEvent.TryCreateLogRow(line, fileProvider, out var logEvent))
                        {
                            LogEvents.Push(logEvent);
                            linesRead++;
                        }
                        else
                        {
                            linesFailed++;
                        }
                    }

                    currentLogPosition = stream.Position;
                }

                LogNames = LogEvents.Select(e => e.LogName).Distinct().ToArray();
                LogLevels = LogEvents.Select(e => e.LogLevel).Distinct().ToArray();

                lastReloadTime = DateTime.Now;

                Console.WriteLine($"Read {linesRead + linesFailed} ({linesRead} successfully).");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed to load log files: {exception.Message}");
            }
        }
    }
}
