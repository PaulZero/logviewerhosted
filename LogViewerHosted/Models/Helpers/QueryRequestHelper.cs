﻿using Microsoft.AspNetCore.Http;
using System;
using System.Globalization;

namespace LogViewerHosted.Models.Helpers
{
    public class QueryRequestHelper
    {
        public const uint DefaultPageNumber = 1;

        public const uint DefaultPageSize = 25;

        public int PageNumber { get; private set; }

        public int PageSize { get; private set; }

        public string LogName { get; private set; }

        public string LogLevel { get; private set; }

        public string SearchText { get; private set; }

        public DateTime StartDate { get; private set; }

        public DateTime EndDate { get; private set; }

        public bool HasLogName => !string.IsNullOrWhiteSpace(LogName);

        public bool HasLogLevel => !string.IsNullOrWhiteSpace(LogLevel);

        public bool HasSearchText => !string.IsNullOrWhiteSpace(SearchText);

        public bool HasEndDate => EndDate != default(DateTime);

        private IQueryCollection QueryCollection { get; }

        public QueryRequestHelper(IQueryCollection queryCollection)
        {
            QueryCollection = queryCollection;

            ReadValues();
        }

        private void ReadValues()
        {
            PageNumber = (int)GetUint("pageNumber", DefaultPageNumber) - 1;
            PageSize = (int)GetUint("pageSize", DefaultPageSize); 
            LogName = GetString("logName");
            LogLevel = GetString("logLevel");
            SearchText = GetString("searchText")?.ToLower();
            StartDate = GetDateTime("startDate", DateTime.Now.AddDays(-1));
            EndDate = GetDateTime("endDate").AddDays(1);
        }

        private bool Has(string key)
        {
            return QueryCollection.ContainsKey(key);
        }

        private string GetString(string key, string defaultValue = default(string))
        {
            if (!Has(key))
            {
                return defaultValue;
            }

            return QueryCollection[key].ToString();
        }

        private uint GetUint(string key, uint defaultValue = default(uint))
        {
            if (!Has(key))
            {
                return defaultValue;
            }

            if (uint.TryParse(GetString(key), out var parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }

        private DateTime GetDateTime(string key, DateTime defaultValue = default(DateTime))
        {
            if (!Has(key))
            {
                return defaultValue;
            }

            if (DateTime.TryParse(GetString(key), out var parsedValue))
            {
                Console.WriteLine($"Extracted date from request: {parsedValue}");

                return parsedValue;
            }
            else
            {
                Console.WriteLine($"Could not parse datetime value: ${GetString(key)}");
            }

            return defaultValue;
        }
    }
}
