﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LogViewerHosted.Models
{
    public class LogEvent
    {
        [JsonIgnore]
        public static Regex LogRowPattern = new Regex(
            @"^\[(?<timestamp>\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}\:\d{2})\] (?<logName>[a-zA-Z\-_]{1,})\.(?<logLevel>[A-Z]{1,})\: (?<logText>.*)$");

        [JsonIgnore]
        public static Regex LogTextJsonPattern = new Regex(
            @"^(?<message>.*) (?<json>[\{\(]{1}.*[\}\]]{1})$");

        [JsonProperty("id")]
        public Guid Id { get; }

        [JsonIgnore]
        public DateTime Timestamp { get; }

        [JsonProperty("timestamp")]
        public string FormattedTimestamp => Timestamp.ToString("G");

        [JsonProperty("logName")]
        public string LogName { get; }

        [JsonProperty("logLevel")]
        public string LogLevel { get; }

        [JsonProperty("logText")]
        public string LogText { get; }

        [JsonIgnore]
        public string Json { get; }

        [JsonIgnore]
        public StackTrace Trace { get; private set; }

        [JsonProperty("hasJson")]
        public bool HasJson => !string.IsNullOrWhiteSpace(Json);

        [JsonProperty("hasStackTrace")]
        public bool HasTrace => Trace is StackTrace;

        [JsonIgnore]
        public string SearchText { get; }

        public LogEvent(DateTime timestamp, string logName, string logLevel, string logText, IFileProvider fileProvider)
        {
            Id = Guid.NewGuid();
            Timestamp = timestamp;
            LogName = logName;
            LogLevel = logLevel;
            LogText = logText.Trim();

            // Get the last index of [ then substring to that to remove the dead [] and the space before it.
            LogText = LogText.Substring(0, LogText.LastIndexOf('[') - 1);

            // if string ends in [] now then fuck that off too.
            if (LogText.EndsWith(" []"))
            {
                LogText = LogText.Substring(0, LogText.LastIndexOf('[') - 1);
            }
            else
            {
                // Check to see if it contains some JSON, if so we can populate that as well..

                if (LogText.Contains("{") && LogText.Contains("}"))
                {
                    var startOfJson = LogText.IndexOf("{", StringComparison.Ordinal);
                    var rawJson = LogText.Substring(startOfJson);
                    LogText = LogText.Substring(0, startOfJson - 1);

                    if (JsonConvert.DeserializeObject(rawJson) is JToken actualJson)
                    {
                        var passwordPaths = new List<string>();

                        ParseJson(actualJson, passwordPaths, fileProvider, out var stackTracePath);

                        // If a stack trace exists within the log row, purge it so we're not sending
                        // it back twice in the response!
                        if (HasTrace && actualJson?.SelectToken(stackTracePath) is JValue stackTraceValue)
                        {
                            stackTraceValue.Value = "...";
                        }

                        // For each password that was located within the JSON ensure that it's purged
                        // so they cannot be harvested from the logs.
                        foreach (var passwordPath in passwordPaths)
                        {
                            if (actualJson?.SelectToken(passwordPath) is JValue passwordValue)
                            {
                                passwordValue.Value = "...";
                            }
                        }

                        Json = actualJson.ToString(Formatting.Indented);
                    }
                }
            }

            SearchText = (LogText + Json).ToLower();
        }

        public bool ContainsString(string value)
        {
            return SearchText.Contains(value.ToLower());
        }

        public static bool TryCreateLogRow(string rawLogRow, IFileProvider fileProvider, out LogEvent logRow)
        {
            var match = LogRowPattern.Match(rawLogRow);

            if (match.Success)
            {
                var timestamp = DateTime.Parse(match.Groups["timestamp"].Value);
                var logName = match.Groups["logName"].Value;
                var logLevel = match.Groups["logLevel"].Value;
                var logText = match.Groups["logText"].Value;

                logRow = new LogEvent(timestamp, logName, logLevel, logText, fileProvider);

                return true;
            }

            logRow = null;

            return false;
        }

        private void ParseJson(JToken token, ICollection<string> passwordPaths, IFileProvider fileProvider, out string stackTracePath)
        {
            stackTracePath = "";

            if (token is JProperty property)
            {
                if (property.Value is JValue value)
                {
                    var name = property.Name.ToLower();

                    if (new[] { "stacktrace", "stack trace" }.Contains(name))
                    {
                        Trace = new StackTrace(value.Value?.ToString(), fileProvider);

                        stackTracePath = property.Path;
                    }

                    if (name == "password")
                    {
                        passwordPaths.Add(property.Path);
                    }

                    return;
                }
            }

            foreach (var childToken in token.Children())
            {
                ParseJson(childToken, passwordPaths, fileProvider, out stackTracePath);
            }
        }
    }
}
