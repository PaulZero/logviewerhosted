﻿using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogViewerHosted.Models
{
    public class StackTrace
    {
        [JsonIgnore]
        public string RawStackTrace { get; }

        [JsonProperty("lines")]
        public StackTraceLine[] Lines
        {
            get
            {
                if (innerLines == null)
                {
                    innerLines = ExtractLines();
                }

                return innerLines;
            }
        }

        private StackTraceLine[] innerLines;

        private IFileProvider fileProvider;

        public StackTrace(string rawStackTrace, IFileProvider fileProvider)
        {
            RawStackTrace = rawStackTrace;

            this.fileProvider = fileProvider;
        }

        private StackTraceLine[] ExtractLines()
        {
            return RawStackTrace.Split('\n').Select(l => new StackTraceLine(l, fileProvider)).ToArray();
        }
    }
}
