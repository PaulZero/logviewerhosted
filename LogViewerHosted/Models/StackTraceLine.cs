﻿using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LogViewerHosted.Models
{
    public class StackTraceLine
    {
        /// <summary>
        /// The number of lines to show either side of a line of code located in a stack trace.
        /// </summary>
        public const int SourcePreviewLines = 5;

        [JsonIgnore]
        public readonly Regex FilePathRegex = new Regex(@"(?<filePath>[\/a-zA-Z\ ]{1,}\.php)\((?<lineNumber>[0-9]{1,})\)");

        [JsonProperty("text")]
        public string Line { get; }

        [JsonIgnore]
        public string CodeFile { get; }

        [JsonIgnore]
        public int CodeLineNumber { get; }

        [JsonProperty("sourceLines")]
        public string[] SourceLines { get; }

        [JsonProperty("hasSource")]
        public bool HasSource => (SourceLines?.Count() ?? 0) > 0;

        [JsonProperty("sourceStartLine")]
        public int SourceStartLine { get; }

        public StackTraceLine(string line, IFileProvider fileProvider)
        {
            Line = line;

            var match = FilePathRegex.Match(line);

            if (match.Success && int.TryParse(match.Groups["lineNumber"].Value, out int lineNumber))
            {
                CodeFile = match.Groups["filePath"].Value;
                CodeLineNumber = lineNumber;

                if (CodeFile.StartsWith('/'))
                {
                    CodeFile = CodeFile.Substring(1);
                }

                var fileInfo = fileProvider.GetFileInfo(CodeFile.Substring(1));

                if (fileInfo.Exists)
                {
                    using (var stream = fileInfo.CreateReadStream())
                    using (var reader = new StreamReader(stream))
                    {
                        var allText = reader.ReadToEnd();
                        var allLines = allText.Split('\n');
                        var zeroIndexedLineNumber = CodeLineNumber - 1;

                        SourceStartLine = CodeLineNumber - SourcePreviewLines;
                        SourceLines = allLines.Skip(zeroIndexedLineNumber - SourcePreviewLines).Take((SourcePreviewLines * 2) + 1).ToArray();
                    }
                }
            }
        }
    }
}
