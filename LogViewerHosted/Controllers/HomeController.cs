﻿using LogViewerHosted.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LogViewerHosted.Controllers
{
    public class HomeController : Controller
    {
        private readonly LogMonitorService logService;

        public HomeController(LogMonitorService logService)
        {
            this.logService = logService;
        }

        [Route("/")]
        public async Task<IActionResult> Viewer()
        {
            await logService.GetLogEventsAsync();

            return View();
        }        
    }
}
