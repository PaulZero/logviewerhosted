﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogViewerHosted.Models;
using LogViewerHosted.Models.Helpers;
using LogViewerHosted.Services;
using Microsoft.AspNetCore.Mvc;

namespace LogViewerHosted.Controllers
{
    public class LogQueryController : Controller
    {
        private readonly LogMonitorService logService;

        public LogQueryController(LogMonitorService logService)
        {
            this.logService = logService;
        }

        [Route("/meta-data")]
        public IActionResult GetMetaData()
        {
            return Json(new
            {
                ok = true,
                data = new
                {
                    names = logService.LogNames,
                    levels = logService.LogLevels
                }
            });
        }

        [Route("/query-log-events")]
        public async Task<IActionResult> QueryLogEvents()
        {
            var requestHelper = new QueryRequestHelper(Request.Query);

            var currentLogEvents = await logService.GetLogEventsAsync();
            var responseLogEvents = new List<LogEvent>();

            foreach (var logEvent in currentLogEvents)
            {
                if (requestHelper.HasLogName && logEvent.LogName != requestHelper.LogName)
                {
                    continue;
                }

                if (requestHelper.HasLogLevel && logEvent.LogLevel != requestHelper.LogLevel)
                {
                    continue;
                }

                if (logEvent.Timestamp < requestHelper.StartDate)
                {
                    continue;
                }

                if (requestHelper.HasEndDate && logEvent.Timestamp > requestHelper.EndDate)
                {
                    continue;
                }

                if (requestHelper.HasSearchText && !logEvent.SearchText.Contains(requestHelper.SearchText))
                {
                    continue;
                }

                responseLogEvents.Add(logEvent);
            }

            var pageNumber = requestHelper.PageNumber + 1;
            var totalRows = responseLogEvents.Count();
            var totalPages = (int)Math.Floor((double)totalRows / requestHelper.PageSize);

            return Json(new
            {
                ok = true,
                data = responseLogEvents.Skip(requestHelper.PageNumber * requestHelper.PageSize).Take(requestHelper.PageSize).ToArray(),
                paging = new
                {
                    pageNumber,
                    totalPages,
                    totalRows
                }
            });
        }

        [Route("/single-log-event/{id}")]
        public async Task<IActionResult> GetSingleLogEvent(Guid id)
        {
            var logEvents = await logService.GetLogEventsAsync();

            var logEvent = logEvents.FirstOrDefault(l => l.Id.Equals(id));

            if (logEvent is null)
            {
                return NotFound();
            }

            return Json(new
            {
                ok = true,
                json = logEvent.Json,
                stackTrace = logEvent.Trace
            });
        }
    }
}